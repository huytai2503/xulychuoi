package khtn.edu.vn;

import java.util.Scanner;
import java.util.StringTokenizer;

public class HocXuLyChuoi {

	public static void main(String[] args) {
		String ten = "";
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhap vao ten ban: ");
		ten = sc.nextLine();
		System.out.println("Chao ban "+ten);
		sc.close();
		System.out.println("So luong ky tu hoa = "+slUpperCase(ten));
		System.out.println("So luong ky tu thuong = "+slLowerCase(ten));
		System.out.println("So luong khoang trang = "+slWhiteSpace(ten));
		StringToken(ten);
		
		String name = "";
		StringBuilder builder = new StringBuilder();
		builder.append("Tran ");
		builder.append("Huy ");
		builder.append("Tai");
		name = builder.toString();
		System.out.println(name);
	}

	private static void StringToken(String s)
	{
//		String delim = "@";
//		StringTokenizer token =  new StringTokenizer(s,delim);
		StringTokenizer token =  new StringTokenizer(s);
		while(token.hasMoreTokens())
		{
			String str = token.nextToken();
			System.out.println(str);
		}
	}
	
	private static int slWhiteSpace(String s) 	
	{
		char [] arr = s.toCharArray();
		int sum = 0;
		for(int i = 0;i<arr.length;i++)
		{
			if(Character.isWhitespace(arr[i]))
			{
				sum++;
			}
		}
		return sum;
	}
	
	private static int slLowerCase(String s) 
	{
		char [] arr = s.toCharArray();
		int sum = 0;
		for(int i = 0;i<arr.length;i++)
		{
			if(Character.isLowerCase(arr[i]))
			{
				sum++;
			}
		}
		return sum;
	}

	public static int slUpperCase(String s) 
	{
		char [] arr = s.toCharArray();
		int sum = 0;
		for(int i = 0;i<arr.length;i++)
		{
			if(Character.isUpperCase(arr[i]))
			{
				sum++;
			}
		}
		return sum;
	}
}

//Bai tap ve nha:
//Bien chuoi bat ky thanh quy dinh ve viet ten
//